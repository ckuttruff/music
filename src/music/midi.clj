(ns music.midi
  (:use [overtone.live]
        [overtone.synth.sampled-piano :only [sampled-piano]])
  (:require [music.samples :as samples]))

(defn note-event [sample-map]
  (on-event [:midi :note-on]
            (fn [midi-note]
              (let [{note :note vel :velocity} midi-note
                    sample (sample-map note)]
                (if sample
                  (samples/play sample)
                  (sampled-piano :note note :amp (/ vel 100) :sustain 0.1))))
            ::q-midi))
