(ns music.percussion
  (:use [overtone.live]
        [overtone.inst.drum])
  (:require [music.samples :as samples]))

(def nome (metronome 85))
(def beats-per-measure 4)

(defn beat-seq [init step total]
  (take total (iterate #(+ step %) init)))

(def beat-map
  {soft-hat (beat-seq 0 0.25 16)
   (samples/sample-fn :fs-kick) [0 2 ]
   (samples/sample-fn :fs-snap) [1 3]})

(defn rhythm
  ([]  (rhythm nome (nome)))
  ([m] (rhythm m (m)))
  ([m beat-num]
   (->> (seq beat-map)
        (map (fn [[f beats]]
               (map #(at (m (+ % beat-num)) (f)) beats)))
        flatten
        doall)
   (apply-at (m (+ beats-per-measure beat-num)) rhythm m (+ beats-per-measure beat-num) [])))
