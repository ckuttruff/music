(ns music.samples
  (:use [overtone.live]))

(defn rp [fname]
  (str "resources/" fname))

(def sample-paths
  {:kate-bush    (rp "kate_bush.wav")
   :snare-roll-1 (rp "egospect_fades_snare_drum1.wav")
   :snare-roll-2 (rp "egospect_fades_snare_drum2.wav")
   :snare-roll-3 (rp "egospect_fades_snare_drum3.wav")
   :fs-kick  (freesound-path 428)
   :fs-snare (freesound-path 26903)
   :fs-snap  (freesound-path 87731)})

(defn sample-fn [k]
  (let [path (sample-paths k)]
    (sample path)))

(defn play [k]
  ((sample-fn k)))
