(ns music.synths
  (:use [overtone.live]))

(definst sin-wave [freq 440 attack 0.1 sustain 1 release 0.1 vol 0.4]
  (* (env-gen (lin attack sustain release) 1 1 0 1 FREE)
     (sin-osc freq)
     vol))

(definst sin-wave' [freq 440 attack 0.1 sustain 1 release 0.1 vol 0.4]
  (* (env-gen (lin attack sustain release))
     (sin-osc freq)
     vol))

